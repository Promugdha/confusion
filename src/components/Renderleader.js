import React from 'react';
import { Media } from 'reactstrap';


const Leader = (props) => {
    const lead = props.leade.map((leads) => {
        return (
            <>
                <Media className="m-2">
                    <Media left className="ml-5 mr-5 mt-2">
                        <Media object src={"/assets/" + leads.image} alt={leads.id} />
                    </Media>
                    <Media body>
                        <Media heading>{leads.name}</Media>
                        <Media ><h6>{leads.designation}</h6></Media>
                        <Media>{leads.description}</Media>
                    </Media>
                </Media>
                <br />
            </>
        )
    })
    return (
        <>
            {lead}
        </>
    )

}

export default Leader;