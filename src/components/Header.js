
import React, { Component } from 'react';
import { Nav, NavItem, Collapse, NavbarToggler, Navbar, NavbarBrand, Jumbotron, Modal, ModalBody, ModalHeader, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { NavLink } from 'react-router-dom';
class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            togglenav: false,
            togglemodal: false
        }
        this.changer = this.changer.bind(this)
        this.changerM = this.changerM.bind(this)
        this.handler = this.handler.bind(this)
    }
    changer() {
        this.setState({ togglenav: !this.state.togglenav })
    }
    changerM() {
        this.setState({ togglemodal: !this.state.togglemodal })
    }
    handler(event) {
        this.changerM();
        alert("username" + this.Un.value + "password" + this.Pa.value + "checked" + this.Cb.checked);
        event.preventDefault();
    }
    render() {
        return (
            <>
                <Navbar dark expand='md'>
                    <div className="container">
                        <NavbarToggler onClick={this.changer} />
                        <NavbarBrand href="/"><img src='../assets/images/logo.png' width='41' height='30' alt='res' /> </NavbarBrand>
                        <Collapse isOpen={this.state.togglenav} navbar>
                            <Nav navbar>
                                <NavItem>
                                    <NavLink className='nav-link' to='/home'>
                                        <span className='fa fa-home fa-lg'></span> Home
                            </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className='nav-link' to='/aboutus'>
                                        <span className='fa fa-info fa-lg'></span> About
                            </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className='nav-link' to='/menu'>
                                        <span className='fa fa-list fa-lg'></span> Menu
                            </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className='nav-link' to='/contactus'>
                                        <span className='fa fa-address-card fa-lg'></span> Contact
                            </NavLink>
                                </NavItem>
                            </Nav>
                        </Collapse>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <Button onClick={this.changerM}>
                                    <span className="fa fa-sign-in fa-lg"></span> LogIn
                              </Button>
                            </NavItem>
                        </Nav>
                    </div>
                </Navbar>
                <Jumbotron>
                    <div className='container'>
                        <div className='row row-header'>
                            <div className='col-12 col-sm-6'>
                                <h1>Ristorante Con Fusion</h1>
                                <p>We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!</p>
                            </div>
                        </div>
                    </div>
                </Jumbotron>
                <Modal isOpen={this.state.togglemodal} toggle={this.changerM} >
                    <ModalHeader toggle={this.changerM} >LogIn</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handler}>
                            <FormGroup>
                                <Label>UserName:</Label>
                                <Input type="text" name="Un" id="Un" innerRef={(input) => this.Un = input} />
                            </FormGroup>
                            <FormGroup>
                                <Label>Password:</Label>
                                <Input type="password" name="Pa" id="Pa" innerRef={(input) => this.Pa = input} />
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="checkbox" name="Cb" innerRef={(input) => this.Cb = input} /> Remember Me
                                </Label>
                            </FormGroup>
                            <Button type="submit" value="submit" color="primary" >Sumbit</Button>
                        </Form>
                    </ModalBody>
                </Modal>
            </>
        )
    }
}

export default Header;
