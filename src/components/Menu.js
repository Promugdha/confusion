import React, { Component } from 'react';
import { Card, CardImg, CardImgOverlay, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import Loading from './LoadingSpin';
import { baseurl } from '../shared/baseurl';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    /*dishandler(dish) {
        if (dish != null) {
            return (
                <Card>

                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card>
            )
        }
        else {
            return (
                <div></div>);
        }
    }*/

    render() {

        const menu = this.props.dishes.dish.map((dish) => {
            return (
                <div key={dish.id} className="col-12 col-md-5 mt-5">
                    <Link to={`/menu/${dish.id}`}>
                        <Card>
                            <CardImg width="100%" object src={baseurl + dish.image} alt={dish.name} />
                            <CardImgOverlay className="ml-5">
                                <CardTitle heading>{dish.name}</CardTitle>
                            </CardImgOverlay>
                        </Card>
                    </Link>
                </div>
            );
        });
        // console.log(this.state.selected)
        if (this.props.dishes.isLoading) {
            return (
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            )
        }
        if (this.props.dishes.err) {
            return (
                <div className="container">
                    <div className="row">
                        <h4>{this.props.dishes.err}</h4>
                    </div>
                </div>

            )
        }
        else {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <Breadcrumb>
                                <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
                                <BreadcrumbItem active>Menu</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                        <div className="col-12">
                            <h3>Menu</h3>
                            <hr />
                        </div>
                    </div>

                    <div className="row">

                        {menu}

                    </div>

                </div>
            );
        }
    }
}

export default Menu;