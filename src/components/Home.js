import React from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import Loading from './LoadingSpin';
import { baseurl } from '../shared/baseurl';
import { FadeTransform } from 'react-animation-components';

const Renderhome = ({ items, Load, err }) => {
    if (Load) {
        return (
            <Loading />
        )
    }
    else if (err) {
        return (
            <h4>{err}</h4>
        )
    }
    else {
        return (
            <FadeTransform in transformProps={{ exitTransform: 'scale(0.5) translateY(-50%)' }}>
                <Card>
                    <CardImg width='100%' object src={baseurl + items.image} alt={items.name} />
                    <CardBody>
                        <CardTitle>{items.name}</CardTitle>
                        {items.designation ? <CardSubtitle>{items.designation}</CardSubtitle> : null}
                        <CardText>{items.description}</CardText>
                    </CardBody>
                </Card>
            </FadeTransform>
        )
    }
}

const Home = (props) => {
    return (
        <div className='container'>
            <div className="row align-items-start">
                <div className="col-12 col-md m-1">
                    <Renderhome items={props.dish} Load={props.dishLoading} err={props.disherr} />
                </div>
                <div className="col-12 col-md m-1">
                    <Renderhome items={props.promo} Load={props.promoLoading} err={props.promoerr} />
                </div>
                <div className="col-12 col-md m-1">
                    <Renderhome items={props.lead} Load={props.leadLoading} err={props.leaderr} />
                </div>
            </div>
        </div>
    )
}

export default Home;