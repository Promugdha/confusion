import React from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import Comments from './Commentform';
import Loading from './LoadingSpin'
import { baseurl } from '../shared/baseurl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const RenderDish = ({ dishe, che, post, IDE }) => {
    // console.log(che)


    const dis = che.map((items) => {
        return (
            <div>
                <Fade in>

                    <p>{items.comment}</p>
                    <p>--{items.author} , {new Date(items.date).toDateString()} </p>

                </Fade>
            </div>
        )
    })

    return (
        <>
            <div key={dishe.id} className="col-12 col-md-5 ">
                <FadeTransform in transformProps={{ exitTransform: 'scale(0.5) translateY(-50%)' }}>
                    <Card>
                        <CardImg width="100%" object src={baseurl + dishe.image} alt={dishe.name} />
                        <CardBody>
                            <CardTitle>{dishe.name}</CardTitle>
                            <CardText>{dishe.description}</CardText>
                        </CardBody>
                    </Card>
                </FadeTransform>
            </div>
            <div className="col-12 col-md-5">
                <h1>Comments</h1>
                <Stagger in>
                    {dis}
                </Stagger>
                <Comments post={post} IDE={IDE} />
            </div>
        </>

    )

}




const Dish = (props) => {
    // console.log(props.hand)
    if (props.Loading) {
        return (
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        )
    }
    else if (props.err !== null) {
        return (
            <div className="container">
                <div className="row">
                    <h4>{props.err}</h4>
                </div>
            </div>
        )
    }
    else if (props.dish !== null) {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                            <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <RenderDish dishe={props.dish} che={props.comment} post={props.postComment} IDE={props.dish.id} />
                </div>
            </div>
        )
    }
    else {
        return (<div></div>)
    }
}




export default Dish;