import React, { Component } from 'react';
import { Button, Modal, ModalBody, ModalHeader, Label, Col, Row } from 'reactstrap';
import { LocalForm, Control, Errors } from 'react-redux-form';

const required = (val) => val && val.length
const maxlength = (len) => (val) => !val || val.length <= len
const minlength = (len) => (val) => !val || val.length >= len



class Comments extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tog: false
        }
        this.changer = this.changer.bind(this);
        this.submitter = this.submitter.bind(this);
    }

    changer() {
        this.setState({ tog: !this.state.tog })
    }
    submitter(value) {
        this.changer();
        this.props.post(this.props.IDE, value.star, value.Name, value.Com)
    }

    render() {
        return (
            <>
                <Button onClick={this.changer} >
                    <span className="fa fa-pencil fa-lg"></span> Submit Comment</Button>
                <Modal isOpen={this.state.tog} toggle={this.changer}>
                    <ModalHeader toggle={this.changer}>
                        Submit Comment
                     </ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(value) => this.submitter(value)}>
                            <Row className="form-group">

                                <Col>
                                    <Label>Rating</Label>
                                    <Control.select model=".star" name="star" id="star" className="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Control.select>
                                </Col>

                            </Row>
                            <Row className="form-group">
                                <Col>
                                    <Label>Author Name</Label>
                                    <Control.text model=".Name" name="Name" id="Name" className="form-control" placeholder="Your Name" validators={{ required, minlength: minlength(3), maxlength: maxlength(13) }} />
                                    <Errors
                                        className="text-danger"
                                        model=".Name"
                                        show="touched"
                                        messages={{
                                            required: '*Required',
                                            minlength: ' *Minimum character length is three',
                                            maxlength: ' *Maximum character length is thirteen'
                                        }} />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col>
                                    <Label>Comment</Label>
                                    <Control.textarea model=".Com" name="Com" id="Com" rows="8" className="form-control" placeholder="Say a word or two" />
                                </Col>
                            </Row>
                            <Button color="primary" type="submit" >Submit </Button>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </>
        )
    }
}


export default Comments;