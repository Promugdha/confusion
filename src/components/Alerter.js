import React, { useState } from 'react';
import { Alert } from 'reactstrap';

const AlertE = (props) => {
    const [visible, setVisible] = useState(true);

    const onDismiss = () => setVisible(false);
    if (props.alerter !== undefined) {
        return (
            <Alert color="info" isOpen={visible} toggle={onDismiss}>
                <h3>hello</h3>
                <p>{props.alerter.firstname} {props.alerter.lastname}</p>
                <h3>your number</h3>
                <p>{props.alerter.telnum}</p>
                <h3>your email</h3>
                <p>{props.alerter.email}</p>
                <h3>your id</h3>
                <p>{props.alerter.id}</p>
            </Alert>
        )
    }
    else {
        console.log("no")
        return (
            <></>
        )
    }
}

export default AlertE;