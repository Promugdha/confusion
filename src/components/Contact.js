import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Button, Label, Col, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors, actions } from 'react-redux-form';
import AlertE from './Alerter';


const required = (val) => val && val.length
const maxlenth = (len) => (val) => !val || val.length <= len
const minlength = (len) => (val) => !val || val.length >= len
const isNumber = (val) => !val || !isNaN(Number(val))
const validemail = (val) => !val || /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i.test(val);




class Contact extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // al: false
        }

        this.submitter = this.submitter.bind(this)

    }
    submitter(values) {
        //console.log(JSON.stringify(values))
        //alert(JSON.stringify(values))

        this.props.formfeedreset()
        this.props.post(values.firstname, values.lastname, values.telnum, values.email, values.agree, values.contactType, values.message)
        //alert(JSON.stringify(this.props.feed.filter((feed) => feed.id === this.props.feed.length)[0]))
        //this.componentDidMount(alerter(JSON.stringify(this.props.feed)))
        // this.componentDidUpdate()
        // this.setState({ al: !this.state.al })
    }
    /* componentDidUpdate() {
         alert(JSON.stringify(this.props.feed))
         console.log(this.props.feed[0])
     }*/

    render() {
        //console.log("in")


        return (
            <>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <Breadcrumb>
                                <BreadcrumbItem><Link to='/menu'>Home</Link></BreadcrumbItem>
                                <BreadcrumbItem active>Contact Us</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                        <div className="col-12">
                            <h3>Contact Us</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row row-content">
                        <div className="col-12">
                            <h3>Location Information</h3>
                        </div>
                        <div className="col-12 col-sm-4 offset-sm-1">
                            <h5>Our Address</h5>
                            <address>
                                121, Clear Water Bay Road<br />
                        Clear Water Bay, Kowloon<br />
                        HONG KONG<br />
                                <i className="fa fa-phone"></i>: +852 1234 5678<br />
                                <i className="fa fa-fax"></i>: +852 8765 4321<br />
                                <i className="fa fa-envelope"></i>: <a href="mailto:confusion@food.net">confusion@food.net</a>
                            </address>
                        </div>
                        <div className="col-12 col-sm-6 offset-sm-1">
                            <h5>Map of our Location</h5>
                        </div>
                        <div className="col-12 col-sm-11 offset-sm-1">
                            <div className="btn-group" role="group">
                                <a role="button" className="btn btn-primary" href="tel:+85212345678"><i className="fa fa-phone"></i> Call</a>
                                <a role="button" className="btn btn-info"><i className="fa fa-skype"></i> Skype</a>
                                <a role="button" className="btn btn-success" href="mailto:confusion@food.net"><i className="fa fa-envelope-o"></i> Email</a>
                            </div>
                        </div>
                    </div>
                    <div className="row row-content">
                        <div className="col-12 col-md-9">
                            <h3>Send Feedback</h3>
                            <br />
                            <LocalForm model="feedback" onSubmit={(values) => this.submitter(values)}>
                                <Row className="form-group">
                                    <Label md={2} htmlFor="firstName">First-Name</Label>
                                    <Col md={10}>
                                        <Control.text model=".firstname" id="FN" name="firstname" placeholder="First Name"
                                            className="form-control"
                                            validators={{ required, maxlenth: maxlenth(13), minlength: minlength(3) }} />
                                        <Errors
                                            className="text-danger"
                                            model=".firstname"
                                            show="touched"
                                            messages={{
                                                required: '*Required',
                                                minlength: ' *The name should have atleast 3 letters',
                                                maxlenth: ' *The name can have atmost 13 letters'
                                            }} />
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Label md={2} htmlFor="lastName">Last-Name</Label>
                                    <Col md={10}>
                                        <Control.text model=".lastname" id="LN" name="lastname" placeholder="Last Name"
                                            className="form-control" validators={{ required, maxlenth: maxlenth(13), minlength: minlength(3) }} />
                                        <Errors
                                            className="text-danger"
                                            model=".lastname"
                                            show="touched"
                                            messages={{
                                                required: '*Required',
                                                minlength: ' *The name should have atleast 3 letters',
                                                maxlenth: ' *The name can have atmost 13 letters'
                                            }} />
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Label md={2} htmlFor="email">Email</Label>
                                    <Col md={10}>
                                        <Control.text model=".email" id="EM" name="email" placeholder="Email"
                                            className="form-control"
                                            validators={{ required, validemail }} />
                                        <Errors
                                            className="text-danger"
                                            model=".email"
                                            show="touched"
                                            messages={{
                                                required: '*Required',
                                                validemail: ' *not a valid email',

                                            }} />
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Label md={2} htmlFor="tel">Tel-Num</Label>
                                    <Col md={10}>
                                        <Control.text model=".telnum" id="TL" name="telnum" placeholder="Tel Num"
                                            className="form-control"
                                            validators={{ required, isNumber, maxlenth: maxlenth(10) }} />
                                        <Errors
                                            className="text-danger"
                                            model=".telnum"
                                            show="touched"
                                            messages={{
                                                required: '*Required',
                                                isNumber: ' *not a valid number',
                                                maxlenth: ' *should have less then 11 characters'
                                            }} />

                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Col md={{ size: 6, offset: 2 }}>
                                        <div className="form-check">
                                            <Label check>
                                                <Control.checkbox model=".agree" name="agree"
                                                    className="form-check-input"
                                                /> {' '}
                                                <strong>May we contact you?</strong>
                                            </Label>
                                        </div>
                                    </Col>
                                    <Col md={{ size: 3, offset: 1 }}>
                                        <Control.select model=".contactType" name="contactType"
                                            className="form-control">
                                            <option>Tel.</option>
                                            <option>Email</option>
                                        </Control.select>
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Label htmlFor="message" md={2}>Your Feedback</Label>
                                    <Col md={10}>
                                        <Control.textarea model=".message" id="message" name="message"
                                            rows="12"
                                            className="form-control" />
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Col md={{ size: 10, offset: 2 }}>
                                        <Button type="submit" color="primary">
                                            Send Feedback
                                    </Button>
                                    </Col>
                                </Row>
                            </LocalForm>
                        </div>
                    </div>
                </div>
                <AlertE alerter={this.props.feed[this.props.feed.length - 1]} />
            </>
        );
    }
}

export default Contact;