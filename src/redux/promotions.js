
import * as ActionTypes from './actiontypes';

export const Promotion = (state = { promotions: [], isLoading: true, err: null }, action) => {
    switch (action.type) {
        case ActionTypes.Promos_fail:
            return { ...state, isLoading: false, err: action.payload, promotions: [] }

        case ActionTypes.Promos_load:
            return { ...state, isLoading: true, err: null, promotions: [] }

        case ActionTypes.Promos_add:
            return { ...state, isLoading: false, err: null, promotions: action.payload }

        default:
            return state;
    }
}