export const ADD_com = 'ADD_com';
export const ADD_feed = 'ADD_feed';
export const Dish_load = 'Dish_load';
export const Dish_fail = 'Dish_fail';
export const Dish_add = 'Dish_add';
export const Com_add = 'Com_add';
export const Com_fail = 'Com_fail';
export const Lead_load = 'Lead_load';
export const Lead_fail = 'Lead_fail';
export const Lead_add = 'Lead_add';
export const Promos_load = 'Promos_load';
export const Promos_fail = 'Promos_fail';
export const Promos_add = 'Promos_add';
export const Feed_load = 'Feed_load';
export const Feed_fail = 'Feed_fail';
export const Feed_add = 'Feed_add';
