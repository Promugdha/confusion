import { LEADERS } from '../shared/leaders';
import * as Actiontypes from './actiontypes';

export const Leaders = (state = { isLoading: true, leaders: [], err: null }, action) => {
    switch (action.type) {
        case Actiontypes.Lead_add:
            return { ...state, leaders: action.payload, err: null, isLoading: false }
        case Actiontypes.Lead_fail:
            return { ...state, leaders: [], err: action.payload, isLoading: false }
        case Actiontypes.Lead_load:
            return { ...state, leaders: [], err: null, isLoading: true }
        default:
            return state
    }
}