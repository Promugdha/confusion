
import * as ActionTypes from './actiontypes';


export const Comments = (state = { comments: [], err: null }, action) => {
    switch (action.type) {
        case ActionTypes.Com_add:
            return { ...state, comments: action.payload, err: null }
        case ActionTypes.Com_fail:
            return { ...state, comments: [], err: action.payload }
        case ActionTypes.ADD_com:
            var comment = action.payload
            return { ...state, comments: state.comments.concat(comment) }
        default:
            return state
    }
}