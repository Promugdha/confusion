import * as ActionTypes from './actiontypes';

import { baseurl } from '../shared/baseurl';

export const leadLoad = () => ({
    type: ActionTypes.Lead_load
})

export const leadAdd = (leads) => ({
    type: ActionTypes.Lead_add,
    payload: leads
})

export const leadFail = (err) => ({
    type: ActionTypes.Lead_fail,
    payload: err
})

export const fetchLeaders = () => (dispatch) => {
    dispatch(leadLoad(true))

    return fetch(baseurl + 'leaders')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error("Error" + response.status + ":" + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                var err = new Error(error.message);
                throw err;
            })
        .then(response => response.json())
        .then(response => dispatch(leadAdd(response)))
        .catch(error => dispatch(leadFail(error.message)))
}

export const addComment = (comment) => ({
    type: ActionTypes.ADD_com,
    payload: comment
});


export const postComment = (dishId, rating, author, comment) => (dispatch) => {
    const newcomment = {
        dishId: dishId,
        rating: rating,
        author: author,
        comment: comment,
        date: new Date().toISOString()
    }
    //newcomment.date = new Date().toISOString();

    return fetch(baseurl + 'comments', {
        method: 'POST',
        body: JSON.stringify(newcomment),
        headers: {
            "Content-type": "application/json"
        },
        credentials: 'same-origin'
    })
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error("Error" + response.status + ":" + response.statusText);
                error.response = response;
                throw error;
            }

        },
            error => {
                var ermess = new Error(error.message);
                throw ermess;
            })
        .then(response => response.json())
        .then(response => dispatch(addComment(response)))
        .catch(error => {
            console.log(error.message);
            alert(`your comment is not added,
              Error :${error.message}`)
        })
}

export const fetchDishes = () => (dispatch) => {
    dispatch(dishLoad(true))

    return fetch(baseurl + 'dishes')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error("Error" + response.status + ":" + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                var ermess = new Error(error.message)
                throw ermess;

            })
        .then(response => response.json())
        .then(dishes => dispatch(dishAdd(dishes)))
        .catch(error => dispatch(dishFail(error.message)))
}

export const dishLoad = () => ({
    type: ActionTypes.Dish_load
});


export const dishAdd = (dish) => ({
    type: ActionTypes.Dish_add,
    payload: dish
});


export const dishFail = (err) => ({
    type: ActionTypes.Dish_fail,
    payload: err
})

export const fetchComments = () => (dispatch) => {
    return fetch(baseurl + 'comments')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error("Error" + response.status + ":" + response.statusText)
                error.response = response;
                throw error;
            }
        },
            error => {
                var ermess = new Error(error.message);
                throw ermess;
            })
        .then(response => response.json())
        .then(comments => dispatch(comAdd(comments)))
        .catch(error => dispatch(comFail(error.message)));
}

export const comAdd = (comment) => ({
    type: ActionTypes.Com_add,
    payload: comment
})

export const comFail = (err) => ({
    type: ActionTypes.Com_fail,
    payload: err
})

export const fetchPromos = () => (dispatch) => {
    dispatch(promosLoad(true))

    return fetch(baseurl + 'promotions')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error("Error" + response.status + ":" + response.statusText)
                error.response = response;
                throw error;
            }
        },
            error => {
                var ermess = new Error(error.message);
                throw ermess;
            })
        .then(response => response.json())
        .then(promotion => dispatch(promosAdd(promotion)))
        .catch(error => dispatch(promosFail(error.message)));
}

export const promosAdd = (pro) => ({
    type: ActionTypes.Promos_add,
    payload: pro
})

export const promosFail = (err) => ({
    type: ActionTypes.Promos_fail,
    payload: err
})

export const promosLoad = () => ({
    type: ActionTypes.Promos_load
})

export const addFeedback = (feed) => ({
    type: ActionTypes.ADD_feed,
    payload: feed
})

export const postFeedback = (firstname, lastname, telnum, email, agree, contactType, msg) => (dispatch) => {
    const newfeed = {
        firstname: firstname,
        lastname: lastname,
        telnum: telnum,
        email: email,
        agree: agree,
        contactType: contactType,
        msg: msg,
        date: new Date().toISOString()
    }
    return fetch(baseurl + 'feedback', {
        method: 'POST',
        body: JSON.stringify(newfeed),
        headers: {
            "Content-type": "application/json"
        },
        credentials: 'same-origin'
    })
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error("Error" + response.status + ":" + response.statusText);
                error.response = response;
                throw error;
            }

        },
            error => {
                var ermess = new Error(error.message);
                throw ermess;
            })
        .then(response => response.json())
        .then(response => dispatch(addFeedback(response)))
        .catch(error => console.log(error.message))

}
