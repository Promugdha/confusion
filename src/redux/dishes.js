import * as ActionTypes from './actiontypes';

export const Dishs = (state = { isLoading: true, err: null, dish: [] }, action) => {
    //console.log(state)
    switch (action.type) {
        case ActionTypes.Dish_fail:
            return { ...state, isLoading: false, err: action.payload, dish: [] }

        case ActionTypes.Dish_load:
            return { ...state, isLoading: true, err: null, dish: [] }

        case ActionTypes.Dish_add:
            return { ...state, isLoading: false, err: null, dish: action.payload }

        default:
            return state
    }
}