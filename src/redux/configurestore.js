import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createForms } from 'react-redux-form';
import { Dishs } from './dishes';
import { Comments } from './comments';
import { Promotion } from './promotions';
import { Leaders } from './leaders';
import { Feedback } from './feedback';
import thunk from 'redux-thunk';
import logger from 'redux-logger'
import { initialfeed } from './forms';


export const Configurestore = () => {
    const store = createStore(
        combineReducers({
            dishes: Dishs,
            comments: Comments,
            promotions: Promotion,
            leaders: Leaders,
            feed: Feedback,
            ...createForms({
                feedback: initialfeed
            })
        }),
        applyMiddleware(thunk, logger)
    );
    return store;
}