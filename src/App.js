import React, { Component } from 'react';
import Home from './components/Home'
import Header from './components/Header';
import Menu from './components/Menu';
import Dish from './components/Dishdetail';
import Footer from './components/Fotter';
import Contact from './components/Contact';
import About from './components/About';
import './App.css';
import { Switch, Redirect, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { postComment, fetchDishes, fetchComments, fetchPromos, fetchLeaders, postFeedback } from './redux/actioncreators';
import { actions } from 'react-redux-form';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

const mapStateToProps = state => {
  return {
    dishes: state.dishes,
    comments: state.comments,
    promotions: state.promotions,
    leaders: state.leaders,
    feed: state.feed
  }
}

const mapDispatchToProps = dispatch => ({
  postComment: (dishID, rating, author, comment) => dispatch(postComment(dishID, rating, author, comment)),
  postFeedback: (firstname, lastname, telnum, email, agree, contacttype, msg) => dispatch(postFeedback(firstname, lastname, telnum, email, agree, contacttype, msg)),
  fetchDishes: () => dispatch(fetchDishes()),
  fetchComments: () => dispatch(fetchComments()),
  fetchPromos: () => dispatch(fetchPromos()),
  fetchLeaders: () => dispatch(fetchLeaders()),
  formfeedreset: () => { dispatch(actions.reset('feedback')) },
})

class App extends Component {
  constructor(props) {
    super(props)

  }
  componentDidMount() {
    this.props.fetchDishes()
    this.props.fetchComments()
    this.props.fetchPromos()
    this.props.fetchLeaders()
  }
  render() {

    const DishID = ({ match }) => {
      return (
        <Dish dish={this.props.dishes.dish.filter((di) => di.id === parseInt(match.params.dishID, 10))[0]}
          Loading={this.props.dishes.isLoading}
          err={this.props.dishes.err}
          comment={this.props.comments.comments.filter((com) => com.dishId === parseInt(match.params.dishID, 10))}
          Comerr={this.props.promotions.err}
          postComment={this.props.postComment} />
      )
    }
    return (


      <div className="App">
        <Header />
        <TransitionGroup>
          <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
            <Switch location={this.props.location}>
              <Route path="/home" component={() => <Home dish={this.props.dishes.dish.filter((di) => di.featured)[0]}
                dishLoading={this.props.dishes.isLoading}
                disherr={this.props.dishes.err}
                promo={this.props.promotions.promotions.filter((pr) => pr.featured)[0]}
                promoLoading={this.props.promotions.isLoading}
                promoerr={this.props.promotions.err}
                lead={this.props.leaders.leaders.filter((le) => le.featured)[0]}
                leadLoading={this.props.leaders.isLoading}
                leaderr={this.props.leaders.err} />} />
              <Route exact path="/menu" component={() => <Menu dishes={this.props.dishes} />} />
              <Route path="/menu/:dishID" component={DishID} />
              <Route path="/aboutus" component={() => <About leaders={this.props.leaders.leaders} />} />
              <Route exact path="/contactus" component={() => <Contact formfeedreset={this.props.formfeedreset} post={this.props.postFeedback} feed={this.props.feed.feed} />} />
              <Redirect to="/home" />
            </Switch>
          </CSSTransition>
        </TransitionGroup>
        <Footer />
      </div>


    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
